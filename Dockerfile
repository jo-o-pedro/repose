FROM node:20 AS base
FROM base AS dependencies

WORKDIR /usr/src/app
COPY package.json ./

RUN npm install

FROM base AS BUILD
WORKDIR /usr/src/app

COPY . .

COPY --from=dependencies /usr/src/app/node_modules ./node_modules
COPY --from=dependencies /usr/src/app/package.json ./package.json

RUN npx prisma generate

RUN npm run build

COPY --from=build /usr/src/app/prisma ./prisma
COPY --from-build /usr/src/app/dist ./dist

EXPOSE 3333

CMD [ "npm", "run", "start:prod"]